#!/bin/bash

# ----------------------------- SCRIPT VARIABLES / VARIÁVEIS DO SCRIPT ----------------------------- #
PPA_LIBRATBAG="ppa:libratbag-piper/piper-libratbag-git"
PPA_LUTRIS="ppa:lutris-team/lutris"
PPA_GRAPHICS_DRIVERS="ppa:graphics-drivers/ppa"

URL_WINE_KEY="https://dl.winehq.org/wine-builds/winehq.key"
URL_PPA_WINE="https://dl.winehq.org/wine-builds/ubuntu/"
URL_AUTOMATHEMELLY="https://github.com/C2N14/AutomaThemely/releases/download/v1.3/python3.6-automathemely_1.3_all.deb"

DOWNLOAD_DIRECTORY="$HOME/Downloads/programs"

PROGRAMS_TO_INSTALL=(
  libkf5plasma-dev 
  libkf5plasmaquick5 
  libkf5activities-dev 
  libkf5coreaddons-dev 
  libkf5guiaddons-dev 
  libkf5dbusaddons-dev 
  libkf5declarative-dev 
  libkf5wayland-dev 
  libkf5package-dev 
  libkf5xmlgui-dev 
  libkf5iconthemes-dev 
  libkf5i18n-dev 
  libkf5notifications-dev 
  libkdecorations2-dev 
  libkf5newstuff-dev 
  libkf5archive-dev 
  libkf5globalaccel-dev 
  libkf5crash-dev 
  libkf5windowsystem-dev 
  libqt5x11extras5-dev 
  libxcb1-dev 
  libsm-dev 
  libqt5core5a 
  libqt5gui5 
  libqt5dbus5 
  cmake 
  cmake-extras 
  build-essential
  gettext 
  rofi 
  python3 
  python3-dbus 
  python3-setproctitle 
  python3-xlib 
  gir1.2-gtk-3.0 
  appmenu-qt 
  appmenu-gtk2-module 
  appmenu-gtk3-module
  latte-dock
  samba
  kdenetwork-filesharing
  git
  npm
  docker
  docker-compose
  docker-containerd
  docker.io
  mysql-server-5.7
  inkscape
  libreoffice-gtk3
  libreoffice-calc
  libreoffice-impress
  libreoffice-writer
  libreoffice-evolution
  libreoffice-style-breeze
  libreoffice-style-elementary
  libreoffice-style-galaxy
  libreoffice-style-oxygen
  libreoffice-l10n-pt-br
  chromium
  steam-installer
  steam-devices
  steam:i386
  lutris
  libvulkan1
  libvulkan1:i386
  libgnutls30:i386
  libldap-2.4-2:i386
  libgpg-error0:i386
  libxml2:i386
  libasound2-plugins:i386
  libsdl2-2.0-0:i386
  libfreetype6:i386
  libdbus-1-3:i386
  libsqlite3-0:i386
)
# ------------------------------------------------------------------------------------- #

# ----------------------------- REQUIREMENTS / REQUISITOS ----------------------------- #
## Removing APT lock / Removendo trava do apt ##
sudo rm /var/lib/dpkg/lock-frontend
sudo rm /var/cache/apt/archives/lock

## Adding and confirming 32 bit architecture / Adicionando e confirmando arquitetura 32 bits ##
sudo dpkg --add-architecture i386

## Updating repos / Atualizando repositórios ##
sudo apt update -y

## Adding third party repos and snap support (Logitech and Nvidia Drivers, Lutris) ##
## Adicionando repositórios terceiros e suporte a snap (Drivers Logitech e Nvidia, Lutris) ##
sudo apt-add-repository "$PPA_LIBRATBAG" -y
sudo add-apt-repository "$PPA_LUTRIS" -y
sudo apt-add-repository "$PPA_GRAPHICS_DRIVERS" -y
wget -nc "$URL_WINE_KEY"
sudo apt-key add winehq.key
sudo apt-add-repository "deb $URL_PPA_WINE bionic main"
# --------------------------------------------------------------------------------------- #

# ----------------------------- EXECUTION / EXECUÇÃO ------------------------------------ #
## Updating repos after adding new repos##
sudo apt update -y

## Download and installation of external programs / Download e instalação de programas externos ##
mkdir "$DOWNLOAD_DIRECTORY"
wget -c "$URL_AUTOMATHEMELLY" -P "$DOWNLOAD_DIRECTORY"

## Installing previously downloaded .deb packages / Instalando pacotes .deb baixados anteriormente ##
sudo dpkg -i $DOWNLOAD_DIRECTORY/*.deb

# Install apt programs / Instalando programas apt
for program_name in ${PROGRAMS_TO_INSTALL[@]}
do
  if ! dpkg -l | grep -q $program_name; then # Will install only if not installed / Só instala se já não estiver instalado
    apt install "$program_name" -y
  else
    echo "[ALREADY INSTALLED / JÀ INSTALADO] - $program_name"
  fi
done


sudo apt install --install-recommends winehq-stable wine-stable wine-stable-i386 wine-stable-amd64 -y

## Installing SNAP packages / Instalando pacotes SNAP ##
sudo snap install spotify
sudo snap install slack --classic
sudo snap install intellij-idea-ultimate --classic
sudo snap install datagrip --classic
sudo snap install code --classic
sudo snap install kdenlive
sudo snap install krita
# ---------------------------------------------------------------------------------------- #

# ----------------------------- POST-INSTALL / PÓS-INSTALAÇÃO ---------------------------- #
## Finishing, updating and cleansing / Finalização, atualização e limpeza ##
sudo apt update 
sudo apt dist-upgrade -y
sudo snap refresh
sudo apt autoclean
sudo apt autoremove -y
# --------------------------------------------------------------------------------------- #

# ------------------------------ Compile / Compiláveis ---------------------------------- #
mkdir Dev/toCompile/git
cd Dev/toCompile/git
git clone https://github.com/tsujan/BreezeEnhanced.git
git clone https://github.com/psifidotos/applet-window-appmenu.git
git clone https://github.com/zzag/kwin-effects-yet-another-magic-lamp.git
git clone https://github.com/Zren/plasma-hud.git

cd ~/Dev/toCompile/git/BreezeEnhanced
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release -DKDE_INSTALL_LIBDIR=lib -DBUILD_TESTING=OFF -DKDE_INSTALL_USE_QT_SYS_PATHS=ON
make
sudo make install

cd ~/Dev/toCompile/git/kwin-effects-yet-another-magic-lamp
mkdir build && cd build
cmake .. / -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr
make
sudo make install

cd ~/Dev/toCompile/git/plasma-hud
sudo mkdir -p /usr/lib/plasma-hud
sudo cp -r usr/lib/plasma-hud /usr/lib/plasma-hud/
sudo mkdir -p /etc/xdg/autostart
sudo cp etc/xdg/autostart/plasma-hud.desktop /etc/xdg/autostart/
kwriteconfig5 --file ~/.config/kwinrc --group ModifierOnlyShortcuts --key Alt "com.github.zren.PlasmaHUD,/PlasmaHUD,com.github.zren.PlasmaHUD,toggleHUD"
qdbus org.kde.KWin /KWin reconfigure

cd ~/Dev/toCompile/git/applet-window-appmenu
sh install.sh

cd ~
# ------------------------------------------------------------------------------------- #

# ---------------------------- Java Install / Instalação do Java ---------------------- #
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk install java 8.0.232-open
sdk install java 13.0.1-open
sdk install Maven
# ------------------------------------------------------------------------------------- #

# --------------------------- Angular Install / Instalação do Angular ----------------- #
sudo npm install -g npm@latest
sudo npm install -g @angular/cli@7.3.9 --save
# ------------------------------------------------------------------------------------- #
