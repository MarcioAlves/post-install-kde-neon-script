# Post Install KDE Neon Script

A script that install some tools, libs and programs automatically by running it after a fresh install of KDE Neon

After downloading it, do chmod a+x postInstallScript.sh and ./postInstallScript.sh, type your SUDO password and wait for the installation process to finish

ATTENTION: This is my personal list of favorites libs, tools and programs (including some development ones), that are available in ubuntu repositories, customize it for your own purposes and to fit your favorite distribution
